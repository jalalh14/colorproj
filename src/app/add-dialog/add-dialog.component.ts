import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { IKeyValuePair } from '../interfaces';

@Component({
  selector: 'app-add-dialog',
  templateUrl: './add-dialog.component.html',
  styleUrls: [ './add-dialog.component.css' ]
})
export class AddDialogComponent implements OnInit {

  constructor(private dialogRef: MatDialogRef<AddDialogComponent>,
    @Inject(MAT_DIALOG_DATA) data: any) {
    this.model = data || { value: "#3f51b5" };
  }

  model: any;

  currentStyle: { selector?: string, params?: IKeyValuePair[] } | null = null;
  currentParam: IKeyValuePair | null = null;
  isEdit: boolean = false;


  ngOnInit(): void {
  }

  close() {
    this.dialogRef.close();
  }

  /** save the color */
  save() {
    this.dialogRef.close(this.model);
  }

  /** add style selector */
  addNewSelector() {
    this.currentStyle = {};
  }

  addNewParam() {
    if (this.currentStyle)
    {
      this.currentStyle.params = this.currentStyle.params || [];
      let param = {} as IKeyValuePair;
      this.currentParam = param;
      //this.currentStyle.params.push(param);
    }
  }

  editSelector(style: any) {
    this.currentStyle = style;
  }

  editParam(param: IKeyValuePair) {
    this.currentParam = param;
  }

  /** delete a style from model */
  deleteStyle(style: any) {
    if (style.selector)
    {
      let ind = (this.model!.styles! as any[]).findIndex(p => p!.selector == style.selector);
      if (ind !== -1)
      {
        this.model!.styles!.splice(ind, 1);
      }
    }

  }

  /** delete a param from style */
  deleteParam(param: IKeyValuePair) {
    if (param.key)
    {
      let ind = this.currentStyle!.params!.findIndex(p => p.key == param.key);
      if (ind !== -1)
      {
        this.currentStyle!.params!.splice(ind, 1);
      }
    }

  }

  /** save a selector */
  saveSelector() {
    if (this.isEdit)
    {
      let rec = (this.model.styles as any[]).find(p => p.selector == this.currentStyle!.selector);
      if (rec)
      {
        rec.selector
      }
    }
    else
    {
      this.model.styles = this.model.styles || [];
      if (this.currentStyle!.selector)
      {
        this.model.styles.push(this.currentStyle);
        this.currentParam = null;
        this.currentStyle = null;
      }
    }

  }

  /** save a param for a selector */
  saveParam() {
    this.currentStyle!.params = this.currentStyle!.params || [];
    if (this.currentParam && this.currentParam.key)
    {
      let param = this.currentStyle?.params.find(p => p.key === this.currentParam!.key);
      if (param)
      {
        param.value = this.currentParam.value;
      }
      else
      {
        this.currentStyle?.params.push(this.currentParam);
      }
      this.currentParam = null;
    }
  }

  cancelNewStyle() {
    this.currentStyle = null;
    this.currentParam = null;
  }

  cancelNewParam() {
    this.currentParam = null;
  }

}
