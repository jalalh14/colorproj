export interface IKeyValuePair {
    key: string;
    value: any;
}

export interface IColorStyleModel {
    selector: string;
    params: IKeyValuePair[];
}

export interface IColorModel {
    label?: string;
    value: string;
    description?: string;
    styles: IColorStyleModel[];
}

export interface ISchmaModel {
    color?: IColorModel[];
}

export interface IConfigModel {
    name?: string;
    version?: string;
    schema: ISchmaModel;
}