import { Component } from '@angular/core';
import { MatDialog, MatDialogConfig } from "@angular/material/dialog";
import { AddDialogComponent } from './add-dialog/add-dialog.component';
import { TitleCasePipe } from '@angular/common';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'color project';

  constructor(private dialog: MatDialog) {

  }


  model: any = { colors: {} };
  //colors:any = {};

  openColorDialog() {
    let comp = this;
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = "100%";

    const dialogRef = this.dialog.open(AddDialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(
      data => {
        if (data && data.name)
        {
          let c = { [this.titleCasePipe.transform(data.name)]: { label: this.titleCasePipe.transform(data.name) + " Color", value: data.value, styles: data.styles, description: data.description } };
          comp.model.colors = { ...comp.model.colors, ...c };
        }
        console.log(data);
      }
    );
  }


  deleteColor(color: string) {
    delete this.model.colors[color];
  }

  // editColor(color: any) {
  //   let comp = this;

  //   const name = Object.keys(color)[0];
  //   let c = color[name];
  //   c = { name, ...c };
  //   const dialogConfig = new MatDialogConfig();

  //   dialogConfig.disableClose = true;
  //   dialogConfig.autoFocus = true;
  //   dialogConfig.width = "100%";

  //   const dialogRef = this.dialog.open(AddDialogComponent, dialogConfig);
  //   dialogRef.afterClosed().subscribe(
  //     data => {
  //       if (data && data.name)
  //       {
  //         let c = { [this.titleCasePipe.transform(data.name)]: { label: this.titleCasePipe.transform(data.name) + " Color", value: data.value, styles: data.styles, description: data.description } };
  //         comp.model.colors = { ...comp.model.colors, ...c };
  //       }
  //       console.log(data);
  //     }
  //   );
  // }


  titleCasePipe = new TitleCasePipe();
  getValueOfColor(obj: any) {
    return obj.value;
  }

}
